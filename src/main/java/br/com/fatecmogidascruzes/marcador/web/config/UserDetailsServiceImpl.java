package br.com.fatecmogidascruzes.marcador.web.config;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.fatecmogidascruzes.marcador.web.data.UserDAO;
import br.com.fatecmogidascruzes.marcador.web.model.Role;
import br.com.fatecmogidascruzes.marcador.web.model.User;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserDAO userRepository;

	private DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
		if (email == null || email.trim().isEmpty()) {
			throw new UsernameNotFoundException("Empty username.");
		}
		User user = userRepository.findByEmail(email);

		if (user == null) {
			throw new UsernameNotFoundException("User " + email + " could not be found.");
		}

		Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
		if (user.getRoles() != null) {
			for (Role role : user.getRoles()) {
				grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
			}
		}

		return new MyUserDetails(user.getEmail(), user.getPassword(), grantedAuthorities, user.getName(),
				formatter.format(user.getCreation()));
	}

}