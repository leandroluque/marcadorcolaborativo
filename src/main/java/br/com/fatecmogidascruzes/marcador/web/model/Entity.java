package br.com.fatecmogidascruzes.marcador.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Entity {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "creation_date")
	private Date creation = new Date();
	@Column(name = "last_update")
	private Date lastUpdate = new Date();
	@Column(name = "enabled")
	private Boolean enabled = true;

	public Entity() {
		super();
	}

	public Entity(Long id, Date creation, Date lastUpdate, Boolean enabled) {
		super();
		this.id = id;
		this.creation = creation;
		this.lastUpdate = lastUpdate;
		this.enabled = enabled;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreation() {
		return creation;
	}

	public void setCreation(Date creation) {
		this.creation = creation;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

}
