package br.com.fatecmogidascruzes.marcador.web.data;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.fatecmogidascruzes.marcador.web.model.Image;
import br.com.fatecmogidascruzes.marcador.web.model.Marking;

public interface MarkingDAO extends JpaRepository<Marking, Long> {

	public List<Marking> findByImage(Image image);

	public Marking findByImageAndDivId(Image image, Long divId);

	public void deleteByDivId(Long divId);

	public void deleteByImage(Image image);

	public Long countByImage(Image image);

}
