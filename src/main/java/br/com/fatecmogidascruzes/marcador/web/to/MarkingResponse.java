package br.com.fatecmogidascruzes.marcador.web.to;

public class MarkingResponse {

	private String status;
	private Long divId;

	public MarkingResponse() {
		super();
	}

	public MarkingResponse(String status) {
		super();
		this.status = status;
	}

	public MarkingResponse(String status, Long divId) {
		super();
		this.status = status;
		this.divId = divId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getDivId() {
		return divId;
	}

	public void setDivId(Long divId) {
		this.divId = divId;
	}

}
