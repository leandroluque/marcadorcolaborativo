package br.com.fatecmogidascruzes.marcador.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.fatecmogidascruzes.marcador.web.service.GalleryService;

@RestController
@RequestMapping("/download/")
public class GalleryDownloadController {

	@Autowired
	private GalleryService galleryService;
	
	@RequestMapping("{id}")
	public byte[] load(@PathVariable Long id, @RequestParam(value = "gallery", defaultValue = "false") Boolean gallery) {
		return galleryService.loadPhoto(id, gallery);
	}
	
}
