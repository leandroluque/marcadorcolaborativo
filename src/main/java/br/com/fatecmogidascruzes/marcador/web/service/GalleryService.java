package br.com.fatecmogidascruzes.marcador.web.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import br.com.fatecmogidascruzes.marcador.web.model.Image;
import br.com.fatecmogidascruzes.marcador.web.model.Marking;
import br.com.fatecmogidascruzes.marcador.web.model.User;

public interface GalleryService {

	public void savePhoto(User user, MultipartFile file);

	public byte[] loadPhoto(Long id, Boolean gallery);

	public List<Image> findEnabled(User user);

	public List<Image> findEnabled();
	
	public List<Image> findEnabledOrderById();
	
	public Long countEnabled(User user);

	public Long countEnabled();

	public Page<Image> findEnabled(User user, int page, int size);

	public Page<Image> findEnabled(int page, int size);

	public List<Image> findAll(User user);

	public List<Image> findAll();

	public void delete(Long id);

	public Image find(Long id);

	public void update(Image image);

	public void addMarking(Marking marking);

	public void deleteMarkingByDivId(Long divId);

	public void deleteMarking(Long id);

	public void updateMarking(Marking marking);

	public Marking findByImageAndDivId(Image image, Long divId);

	public List<Marking> findImageMarkings(Image image);

	public Long countByImage(Image image);

	public void deleteByImage(Image image);
}
