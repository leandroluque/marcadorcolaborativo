package br.com.fatecmogidascruzes.marcador.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.extras.springsecurity5.dialect.SpringSecurityDialect;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.templateresolver.ITemplateResolver;

import br.com.fatecmogidascruzes.marcador.web.storage.FileStorage;
import br.com.fatecmogidascruzes.marcador.web.storage.FileStorageLocal;

@Configuration
public class WebConfig implements WebMvcConfigurer {

	public SpringTemplateEngine templateEngine(ITemplateResolver templateResolver) {
		SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		templateEngine.setTemplateResolver(templateResolver);
		templateEngine.addDialect(new SpringSecurityDialect());

		return templateEngine;
	}

	@Bean
	public FileStorage getStorage() {
		return new FileStorageLocal();
	}

//	public EmbeddedServletContainerCustomizer containerCustomizer() {
//		return (container -> container.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/erro/404"),
//				new ErrorPage(HttpStatus.FORBIDDEN, "/erro/403"),
//				new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/erro/500")));
//	}

//	private int maxUploadSizeInMb = 10 * 1024 * 1024; // 10 MB

//	@Bean
//	public TomcatEmbeddedServletContainerFactory tomcatEmbedded() {
//
//		TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();
//
//		tomcat.addConnectorCustomizers((TomcatConnectorCustomizer) connector -> {
//
//			// configure maxSwallowSize
//			if ((connector.getProtocolHandler() instanceof AbstractHttp11Protocol<?>)) {
//				// -1 means unlimited, accept bytes
//				((AbstractHttp11Protocol<?>) connector.getProtocolHandler()).setMaxSwallowSize(-1);
//			}
//
//		});
//
//		return tomcat;
//
//	}

}
