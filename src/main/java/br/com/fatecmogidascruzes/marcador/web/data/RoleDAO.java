package br.com.fatecmogidascruzes.marcador.web.data;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.fatecmogidascruzes.marcador.web.model.Role;

public interface RoleDAO extends JpaRepository<Role, Long> {
	
}
