package br.com.fatecmogidascruzes.marcador.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.com.fatecmogidascruzes.marcador.web.model.Image;
import br.com.fatecmogidascruzes.marcador.web.model.Marking;
import br.com.fatecmogidascruzes.marcador.web.model.User;
import br.com.fatecmogidascruzes.marcador.web.model.Image.Status;
import br.com.fatecmogidascruzes.marcador.web.service.GalleryService;
import br.com.fatecmogidascruzes.marcador.web.service.UserService;
import br.com.fatecmogidascruzes.marcador.web.to.MarkingTO;

@Controller
@RequestMapping("/galeria")
public class GalleryController {

	@Autowired
	private GalleryService galleryService;

	@Autowired
	private UserService userService;

	@RequestMapping
	public ModelAndView list(@RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
			@RequestParam(name = "size", required = false, defaultValue = "20") Integer size) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User loggedUser = userService.findByEmail(auth.getName());
		ModelAndView modelAndView = new ModelAndView("/gallery/list");
		List<Image> images = null;
		Long total = 0L;
		if (page == -1 || size == -1) {
			if (auth.getName().equals("leandro.luque@gmail.com")) {
				images = galleryService.findEnabled();
				total = galleryService.countEnabled();
			} else {
				images = galleryService.findEnabled(loggedUser);
				total = galleryService.countEnabled(loggedUser);
			}
		} else {
			if (auth.getName().equals("leandro.luque@gmail.com")) {
				images = galleryService.findEnabled(page, size).getContent();
				total = galleryService.countEnabled();
			} else {
				images = galleryService.findEnabled(loggedUser, page, size).getContent();
				total = galleryService.countEnabled(loggedUser);
			}
		}
		for (Image image : images) {
			image.setHowManyMarkings(galleryService.countByImage(image));
		}
		modelAndView.addObject("pages", (int) (Math.ceil(total / size)));
		modelAndView.addObject("images", images);
		modelAndView.addObject("page", page);
		modelAndView.addObject("size", size);
		return modelAndView;
	}

	@RequestMapping(value = "/nova", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> upload(@RequestParam("files[]") MultipartFile[] files) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User loggedUser = userService.findByEmail(auth.getName());
		for (MultipartFile file : files) {
			galleryService.savePhoto(loggedUser, file);
		}
		return ResponseEntity.ok().body(null);
	}

	@RequestMapping("/excluir")
	public ModelAndView delete(@RequestParam(value = "id") Long id, RedirectAttributes redirectAttributes) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User loggedUser = userService.findByEmail(auth.getName());

		Image image = galleryService.find(id);
		if (!image.getUser().equals(loggedUser)) {
			redirectAttributes.addFlashAttribute("error", "Você não pode apagar imagens de outros usuários.");
			return new ModelAndView("redirect:/galeria");
		}

		galleryService.delete(id);
		redirectAttributes.addFlashAttribute("message", "Imagem excluída com sucesso!");
		return new ModelAndView("redirect:/galeria");
	}

	@RequestMapping("/alterar")
	public ModelAndView update(@RequestParam(value = "id") Long id,
			@RequestParam(value = "description") String description, RedirectAttributes redirectAttributes) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User loggedUser = userService.findByEmail(auth.getName());

		Image image = galleryService.find(id);
		if (!image.getUser().equals(loggedUser)) {
			redirectAttributes.addFlashAttribute("error", "Você não pode atualizar imagens de outros usuários.");
			return new ModelAndView("redirect:/galeria");
		}
		image.setDescription(description);
		galleryService.update(image);
		redirectAttributes.addFlashAttribute("message", "Imagem alterada com sucesso!");
		return new ModelAndView("redirect:/galeria");
	}

	@RequestMapping("/marcar/{id}")
	public ModelAndView marcar(@PathVariable Long id) {
		ModelAndView modelAndView = new ModelAndView("/gallery/objectMarker");
		modelAndView.addObject("id", id);
		Image image = this.galleryService.find(id);
		List<Marking> originalMarkings = this.galleryService.findImageMarkings(image);
		Long initialSequence = -1L;
		List<MarkingTO> markings = new ArrayList<>();
		for (Marking marking : originalMarkings) {
			if (marking.getDivId() > initialSequence) {
				initialSequence = marking.getDivId();
			}
			markings.add(new MarkingTO(marking));
		}
		modelAndView.addObject("image", image);
		modelAndView.addObject("markings", markings);
		modelAndView.addObject("initialSequence", ++initialSequence);
		return modelAndView;
	}

	@RequestMapping("/marcarRevisada/{id}")
	public ModelAndView marcarRevisada(@PathVariable Long id) {
		Image image = this.galleryService.find(id);
		image.setStatus(Status.REVISADA);
		galleryService.update(image);
		return marcar(id);
	}

	@RequestMapping("/marcarRevisar/{id}")
	public ModelAndView marcarRevisar(@PathVariable Long id) {
		Image image = this.galleryService.find(id);
		image.setStatus(Status.REVISAR);
		galleryService.update(image);
		return marcar(id);
	}

	@RequestMapping("/marcarValidada/{id}")
	public ModelAndView marcarValidada(@PathVariable Long id) {
		Image image = this.galleryService.find(id);
		image.setStatus(Status.VALIDADA);
		galleryService.update(image);
		return marcar(id);
	}
}
