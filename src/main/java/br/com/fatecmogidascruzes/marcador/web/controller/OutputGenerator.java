package br.com.fatecmogidascruzes.marcador.web.controller;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriteParam;
import javax.imageio.stream.FileImageInputStream;
import javax.imageio.stream.ImageInputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.fatecmogidascruzes.marcador.web.model.Image;
import br.com.fatecmogidascruzes.marcador.web.model.Marking;
import br.com.fatecmogidascruzes.marcador.web.model.Marking.FruitType;
import br.com.fatecmogidascruzes.marcador.web.service.GalleryService;
import ij.IJ;
import ij.ImagePlus;
import ij.plugin.ContrastEnhancer;
import ij.process.ImageProcessor;

@RestController
@RequestMapping("/generate")
public class OutputGenerator {

	private static final String YOLO_DATA_FOLDER = "/home/luque/workspace/marcador/outros/output/yolo";

	private static final String ORIGINAL_IMAGES_FOLDER = "/home/luque/workspace/marcador/fotos-e-BD/fotos-marcadas/";
	private static final String OUTPUT_IMAGES_FOLDER = "/home/luque/workspace/marcador/outros/output-22012019x600x600/tensorflow/";
	private static final String OUTPUT_MARKED_IMAGES_FOLDER = "/home/luque/workspace/marcador/outros/output-22012019x600x600/tensorflow/para-validacao/";

	private static final String OUTPUT_WALTER_FOLDER = "/home/luque/workspace/marcador/outros/output-walter/";

	private static final double PROBABILITY_OF_BEING_TRAINED_IMAGE = 0.9;
	private static final double PROBABILITY_OF_RETAIN_BLANK_IMAGE = 0.05;

	private final GalleryService galleryService;

	private int[] selectedImages = { /* 99, */ 362, 363, 366, 368, 370, 397, /* 408, */ 426, 427, 429, 437, 438, 447,
			494, 521, 529, 530, 533, 551, 601, 625, 691, 695, 706, 732, 734, 765, 766, 817, 818, 819, 824, 854, 866,
			895, 903, 906, 913, 914, 920, 924, 982, 1013, 1056, 1143, 1149, 1186, 1202, 1214, 1215, 1217, 1218 };

//	private int[] selectedImages = { 370, 368, 397, 426, 444, 522, 532, 619, 824, 823, 817, 838, 836, 841, 1141, 1162,
//			1158, 1154, 1187, 253, 249, 244, 206, 146, 131, 130, 113, 114, 116, 366, 494, 525, 700, 855, 839, 840, 1139,
//			1140, 1218, 1217, 1214, 1212, 1211, 1209, 1218, 1217, 1216, 1215, 1214, 1213, 1210, 1188 };

	private String exclude = "";

//	private String exclude = "image_244-___1x___1024_1024____1_1.jpg\r\n" + "image_206-___1x___1024_1024____1_1.jpg\r\n"
//			+ "image_244-___1x___1024_1024____1_1.jpg\r\n" + "image_253-___1x___1024_1024____1_1.jpg\r\n"
//			+ "image_253-___1x___1024_1024____2_1.jpg\r\n" + "image_366-___1x___1024_1024____1_2.jpg\r\n"
//			+ "image_366-___1x___1024_1024____2_3.jpg\r\n" + "image_366-___1x___1024_1024____1_3.jpg\r\n"
//			+ "image_368-___1x___1024_1024____1_2.jpg\r\n" + "image_368-___1x___1024_1024____2_2.jpg\r\n"
//			+ "image_370-___1x___1024_1024____1_1.jpg\r\n" + "image_370-___1x___1024_1024____2_2.jpg\r\n"
//			+ "image_370-___1x___1024_1024____2_1.jpg\r\n" + "image_370-___1x___1024_1024____1_2.jpg\r\n"
//			+ "image_370-___1x___1024_1024____2_3.jpg\r\n" + "image_397-___1x___1024_1024____2_1.jpg\r\n"
//			+ "image_397-___1x___1024_1024____2_2.jpg\r\n" + "image_397-___1x___1024_1024____2_3.jpg\r\n"
//			+ "image_522-___1x___1024_1024____1_1.jpg\r\n" + "image_522-___1x___1024_1024____2_2.jpg\r\n"
//			+ "image_525-___1x___1024_1024____3_2.jpg\r\n" + "image_525-___1x___1024_1024____1_2.jpg\r\n"
//			+ "image_532-___1x___1024_1024____1_3.jpg\r\n" + "image_619-___1x___1024_1024____2_2.jpg\r\n"
//			+ "image_824-___1x___1024_1024____1_3.jpg\r\n" + "image_836-___1x___1024_1024____1_3.jpg\r\n"
//			+ "image_836-___1x___1024_1024____2_1.jpg\r\n" + "image_838-___1x___1024_1024____1_2.jpg\r\n"
//			+ "image_838-___1x___1024_1024____2_2.jpg\r\n" + "image_839-___1x___1024_1024____2_2.jpg\r\n"
//			+ "image_841-___1x___1024_1024____1_3.jpg\r\n" + "image_855-___1x___1024_1024____2_2.jpg\r\n"
//			+ "image_1140-___1x___1024_1024____2_1.jpg\r\n" + "image_1141-___1x___1024_1024____2_1.jpg\r\n"
//			+ "image_1187-___1x___1024_1024____2_1.jpg\r\n" + "image_1187-___1x___1024_1024____2_2.jpg\r\n"
//			+ "image_1187-___1x___1024_1024____3_1.jpg\r\n" + "image_1187-___1x___1024_1024____3_2.jpg\r\n"
//			+ "image_1211-___1x___1024_1024____3_2.jpg\r\n" + "image_1212-___1x___1024_1024____3_1.jpg\r\n"
//			+ "image_1214-___1x___1024_1024____3_1.jpg\r\n" + "image_1214-___1x___1024_1024____3_2.jpg\r\n"
//			+ "image_1215-___1x___1024_1024____3_1.jpg\r\n" + "image_1215-___1x___1024_1024____3_2.jpg\r\n"
//			+ "image_1216-___1x___1024_1024____2_2.jpg\r\n" + "image_1218-___1x___1024_1024____2_2.jpg\r\n"
//			+ "image_532-___1x___1024_1024____1_1.jpg";

	private static final int MIN_TENSORFLOW_IMAGE_WIDTH = 33;
	private static final int MIN_TENSORFLOW_IMAGE_HEIGHT = 33;

	@Autowired
	public OutputGenerator(GalleryService galleryService) {
		super();
		this.galleryService = galleryService;
	}

	@RequestMapping(value = "/tensorflow")
	public String tensorflow(HttpServletResponse response,
			@RequestParam(name = "width", required = true) int widthSubimage,
			@RequestParam(name = "height", required = true) int heightSubimage,
			@RequestParam(name = "oneClass", required = true, defaultValue = "true") boolean onlyOneClass,
			@RequestParam(name = "zoom", required = true, defaultValue = "1") double zoom) throws IOException {

		Arrays.sort(selectedImages);

		// Set response headers.
		response.setStatus(HttpServletResponse.SC_OK);

		// Get all labeled images from the database.
		List<Image> images = galleryService.findEnabledOrderById();

		// Create strings to store the train and test CSV files.
		// These files contain a reference to images and their data.
		StringBuilder trainContent = new StringBuilder();
		StringBuilder testContent = new StringBuilder();
		StringBuilder pbtxContent = new StringBuilder();

		trainContent.append("class,filename,height,width,xmax,xmin,ymax,ymin");
		trainContent.append("\n");

		testContent.append("class,filename,height,width,xmax,xmin,ymax,ymin");
		testContent.append("\n");

		int counterOfImagesUsed = 0;

		for (Image image : images) {

			String originalImageURL = ORIGINAL_IMAGES_FOLDER + image.getName();

			if (true) {

				Dimension dimension = getImageDim(originalImageURL, "jpg");

				// If the image has been found in the selected images' ids array.
				if (Arrays.binarySearch(selectedImages, image.getId().intValue()) >= 0) {

					int originalImageWidth = (int) dimension.getWidth();
					int originalImageHeight = (int) dimension.getHeight();

					counterOfImagesUsed++;

					System.out.println("Processing image " + (counterOfImagesUsed) + "...");

					// Generate N subimages based on the specified width, height, and zoom.

					// Calculate how many subimages will be generated.
					int howManySubimagesHorizontal = (int) Math.ceil(originalImageWidth * zoom / widthSubimage);
					int howManySubimagesVertical = (int) Math.ceil(originalImageHeight * zoom / heightSubimage);

					// Create an image that will be used for resizing and cropping.
					ImagePlus imagePlus = IJ.openImage(originalImageURL);
					ImageProcessor imageProcessor = imagePlus.getProcessor();
					imageProcessor.setInterpolationMethod(ImageProcessor.BICUBIC);
					if (zoom > 1) {
						imageProcessor = imageProcessor.resize((int) (originalImageWidth * zoom),
								(int) (originalImageHeight * zoom));
					}
					imageProcessor.smooth();
					ContrastEnhancer ce = new ContrastEnhancer();
					ce.stretchHistogram(imageProcessor, 0.5);
					imagePlus.updateAndDraw();

					System.out.println("          Creating " + howManySubimagesHorizontal * howManySubimagesVertical
							+ " subimages...");

					for (int x = 0; x < howManySubimagesHorizontal; x++) {
						for (int y = 0; y < howManySubimagesVertical; y++) {

							// Generate the subimage name.
							String subimageName = "image" + "_" + image.getId() + "-___" + (zoom + "x___")
									+ widthSubimage + "_" + heightSubimage + "____" + (x + 1) + "_" + (y + 1) + ".jpg";

							int x1Subimage = x * widthSubimage;
							int y1Subimage = y * heightSubimage;

							int realSubImageWidth = Math.min(widthSubimage, originalImageWidth - x1Subimage);
							int realSubImageHeight = Math.min(heightSubimage, originalImageHeight - y1Subimage);

							if (!exclude.contains(subimageName) && realSubImageWidth > MIN_TENSORFLOW_IMAGE_WIDTH
									&& realSubImageHeight > MIN_TENSORFLOW_IMAGE_HEIGHT) {

								// Crop the original image and resize it.
								imageProcessor.setRoi(x1Subimage, y1Subimage, realSubImageWidth, realSubImageHeight);
								ImageProcessor resizedCroppedImage = imageProcessor.crop();

								// Save the subimage.
								javax.imageio.ImageWriter writer = ImageIO.getImageWritersBySuffix("jpg").next();
								ImageWriteParam param = writer.getDefaultWriteParam();
								param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT); // Needed see javadoc
								param.setCompressionQuality(1.0F); // Highest quality
								ImageOutputStream outSubImage = ImageIO
										.createImageOutputStream(new File(OUTPUT_IMAGES_FOLDER + subimageName));
								writer.setOutput(outSubImage);
								writer.write(null, new IIOImage(resizedCroppedImage.getBufferedImage(), null, null),
										param);
								outSubImage.flush();
								outSubImage.close();

								BufferedImage subImage = resizedCroppedImage.getBufferedImage();

								// For each marking, check whether the marking is inside the subimage.
								List<Marking> markings = image.getMarkings();

								for (Marking marking : markings) {

									// Calculate the marking absolute position considering the original image size
									// with zoom.
									double x1Marking = marking.getX1() * (originalImageWidth * zoom) / 100;
									double y1Marking = marking.getY1() * (originalImageHeight * zoom) / 100;
									double widthMarking = marking.getWidth() * (originalImageWidth * zoom) / 100;
									double heightMarking = marking.getHeight() * (originalImageHeight * zoom) / 100;

									StringBuilder markingOutput = new StringBuilder();

									// Check whether the marking lies inside the subimage.
									if (x1Marking < x1Subimage + widthSubimage && x1Marking + widthMarking > x1Subimage
											&& y1Marking < y1Subimage + heightSubimage
											&& y1Marking + heightMarking > y1Subimage) {

										// Calculate the relative marking position considering the minimum as zero (0),
										// exclusive, and the maximum as one (1), also exclusive.
										int deltaToGuaranteeNotInclusivity = 1;

										if (x1Marking < x1Subimage) {
											widthMarking -= (x1Subimage + deltaToGuaranteeNotInclusivity - x1Marking);
											x1Marking = x1Subimage + deltaToGuaranteeNotInclusivity;

										}
										if (y1Marking < y1Subimage) {
											heightMarking -= (y1Subimage + deltaToGuaranteeNotInclusivity - y1Marking);
											y1Marking = y1Subimage + deltaToGuaranteeNotInclusivity;
										}
										if (x1Marking + widthMarking > x1Subimage + widthSubimage) {
											widthMarking = x1Subimage + widthSubimage - x1Marking;
										}
										if (y1Marking + heightMarking > y1Subimage + heightSubimage) {
											heightMarking = y1Subimage + heightSubimage - y1Marking;
										}

										x1Marking = (x1Marking - x1Subimage);
										y1Marking = (y1Marking - y1Subimage);

										if ((Math.min(x1Marking, widthMarking - x1Marking) < 7
												|| Math.min(y1Marking, heightMarking - y1Marking) < 7)
												&& widthMarking < 15 || heightMarking < 5) {
											continue;
										}

										// Mark the image.
										Graphics subImageGraphics = subImage.getGraphics();
										subImageGraphics.setColor(Color.RED);
										subImageGraphics.drawRect((int) x1Marking, (int) y1Marking, (int) widthMarking,
												(int) heightMarking);

										// Write the class index.
										if (onlyOneClass || marking.getType() == FruitType.GREEN) {
											markingOutput.append("O");
										} else {
											markingOutput.append("L");
										}
										markingOutput.append(",");
										markingOutput.append(subimageName);
										markingOutput.append(",");

										markingOutput
												.append(String.format(Locale.ENGLISH, "%.8f,%.8f,%.8f,%.8f,%.8f,%.8f\n",
														heightMarking, widthMarking, x1Marking + widthMarking,
														x1Marking, y1Marking + heightMarking, y1Marking));

									}

//									if (0 == markingOutput.length()) {
//										if (Math.random() >= PROBABILITY_OF_RETAIN_BLANK_IMAGE) {
//											new File(OUTPUT_IMAGES_FOLDER + subimageName).delete();
//											continue;
//										}
//									}

									// Randomly SELECT the image for training/test.
									if (Math.random() < PROBABILITY_OF_BEING_TRAINED_IMAGE) {
										trainContent.append(markingOutput.toString());
									} else {
										testContent.append(markingOutput.toString());
									}

								}

								javax.imageio.ImageWriter writer1 = ImageIO.getImageWritersBySuffix("jpg").next();
								ImageWriteParam param1 = writer1.getDefaultWriteParam();
								param1.setCompressionMode(ImageWriteParam.MODE_EXPLICIT); // Needed see javadoc
								param1.setCompressionQuality(1.0F); // Highest quality
								ImageOutputStream outMarkedSubImage = ImageIO
										.createImageOutputStream(new File(OUTPUT_MARKED_IMAGES_FOLDER + subimageName));
								writer1.setOutput(outMarkedSubImage);
								writer1.write(null, new IIOImage(subImage, null, null), param1);
								outMarkedSubImage.close();
							}
						}

					}
				} // ./ Does the image have at least 3000 pixels wide?

				System.out.println("          CREATED!");
			} // Is the image reviewed?

		}

		Path trainFile = Paths.get(OUTPUT_IMAGES_FOLDER, "train.csv");
		Path testFile = Paths.get(OUTPUT_IMAGES_FOLDER, "test.csv");
		Path pbtxFile = Paths.get(OUTPUT_IMAGES_FOLDER, "labels.pbtxt");

		File trainOutputFile = trainFile.toFile();
		trainOutputFile.createNewFile();
		FileWriter out = new FileWriter(trainOutputFile);
		out.write(trainContent.toString());
		out.close();

		File testOutputFile = testFile.toFile();
		testOutputFile.createNewFile();
		out = new FileWriter(testOutputFile);
		out.write(testContent.toString());
		out.close();

		if (onlyOneClass) {
			pbtxContent.append("item {\n" + "  id: 1\n" + "  name: 'O'\n" + "}");
		} else {
			pbtxContent.append("item {\n" + "  id: 1\n" + "  name: 'O'\n" + "}\n");
			pbtxContent.append("item {\n" + "  id: 2\n" + "  name: 'L'\n" + "}");
		}

		File pbtxOutputFile = pbtxFile.toFile();
		pbtxOutputFile.createNewFile();
		out = new FileWriter(pbtxOutputFile);
		out.write(pbtxContent.toString());
		out.close();

		return "Done!";
	}

	@RequestMapping(value = "/yolov3", produces = "application/zip")
	public String zipFiles(HttpServletResponse response,
			@RequestParam(name = "width", required = true) int widthSubimage,
			@RequestParam(name = "height", required = true) int heightSubimage,
			@RequestParam(name = "oneClass", required = true, defaultValue = "true") boolean onlyOneClass,
			@RequestParam(name = "zoom", required = true, defaultValue = "1") int zoom) throws IOException {

		// Set response headers.
		response.setStatus(HttpServletResponse.SC_OK);
		// response.addHeader("Content-Disposition", "attachment;
		// filename=\"yolov3.zip\"");

		// Get all labaled images from the database.
		List<Image> images = galleryService.findEnabledOrderById();

		// Create Strings to store the train and test content txt files.
		// These files contain a reference to images and label files.
		StringBuilder trainContent = new StringBuilder();
		StringBuilder testContent = new StringBuilder();

		// Create a list of all files that will be inserted into the ZIP file.
		ArrayList<File> files = new ArrayList<>();

		int counterOfImagesUsed = 0;

		Arrays.sort(selectedImages);

		for (Image image : images) {

			String originalImageURL = ORIGINAL_IMAGES_FOLDER + image.getName();

			// if (Status.REVISADA == image.getStatus()) {
			if (true) {

				// Check whether the image has at least 3000 pixels wide.
				Dimension dimension = getImageDim(originalImageURL, "jpg");
				// if (dimension.getWidth() > 3000) {
				if (Arrays.binarySearch(selectedImages, image.getId().intValue()) >= 0) {

					int widthOriginal = (int) dimension.getWidth();
					int heightOriginal = (int) dimension.getHeight();

					counterOfImagesUsed++;

					System.out.println("Processing image " + (counterOfImagesUsed) + "...");

					// Generate N subimages based on the specified width, height, and zoom.

					// Calculate how many subimages will be generated.
					int howManySubimagesHorizontal = widthOriginal * zoom / widthSubimage;
					int howManySubimagesVertical = heightOriginal * zoom / heightSubimage;

					// Create an image that will be used for resizing and cropping.
					ImagePlus imagePlus = IJ.openImage(originalImageURL);
					ImageProcessor imageProcessor = imagePlus.getProcessor();
					imageProcessor.setInterpolationMethod(ImageProcessor.BICUBIC);
					if (zoom > 1) {
						imageProcessor = imageProcessor.resize(widthOriginal * zoom, heightOriginal * zoom);
					}

					System.out.println("          Creating " + howManySubimagesHorizontal * howManySubimagesVertical
							+ " subimages...");

					for (int x = 0; x < howManySubimagesHorizontal; x++) {
						for (int y = 0; y < howManySubimagesVertical; y++) {

							// Generate the subimage name.
							String subimageName = "image" + "_" + image.getId() + "-___" + (zoom + "x___")
									+ widthSubimage + "_" + heightSubimage + "____" + (x + 1) + "_" + (y + 1) + ".jpg";

							int x1Subimage = x * widthSubimage;
							int y1Subimage = y * widthSubimage;

							if (widthOriginal - x1Subimage > 200 && heightOriginal - y1Subimage > 200) {

								// Crop the original image and resize it.
								imageProcessor.setRoi(x1Subimage, y1Subimage, widthSubimage, heightSubimage);
								ImageProcessor resizedCroppedImage = imageProcessor.crop();

								// Save the subimage.
								javax.imageio.ImageWriter writer = ImageIO.getImageWritersBySuffix("jpg").next();
								ImageWriteParam param = writer.getDefaultWriteParam();
								param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT); // Needed see javadoc
								param.setCompressionQuality(1.0F); // Highest quality
								ImageOutputStream outSubImage = ImageIO
										.createImageOutputStream(new File(OUTPUT_IMAGES_FOLDER + subimageName));
								writer.setOutput(outSubImage);
								writer.write(null, new IIOImage(resizedCroppedImage.getBufferedImage(), null, null),
										param);
								outSubImage.close();

								Path subimageLabelFile = Paths.get(OUTPUT_IMAGES_FOLDER, subimageName + ".txt");

								StringBuilder subimageLabelContent = new StringBuilder();

								// For each marking, check whether the marking is inside the subimage.
								List<Marking> markings = image.getMarkings();
								for (Marking marking : markings) {

									// Calculate the marking absolute position considering the original image size
									// with zoom.
									double x1Marking = marking.getX1() * (widthOriginal * zoom) / 100;
									double y1Marking = marking.getY1() * (heightOriginal * zoom) / 100;
									double widthMarking = marking.getWidth() * (widthOriginal * zoom) / 100;
									double heightMarking = marking.getHeight() * (heightOriginal * zoom) / 100;
									double xCenterMarking = 0;
									double yCenterMarking = 0;

									// Check whether the marking lies inside the subimage.
									if (((x1Marking > x1Subimage && x1Marking < x1Subimage + widthSubimage)
											|| (x1Marking + widthMarking > x1Subimage
													&& x1Marking + widthMarking < x1Subimage + widthSubimage))
											&& ((y1Marking > y1Subimage && y1Marking < y1Subimage + heightSubimage)
													|| (y1Marking + heightMarking > y1Subimage && y1Marking
															+ heightMarking < y1Subimage + heightSubimage))) {

										// System.out.println("=====");
										// System.out.println(x1Marking);
										// System.out.println(y1Marking);
										// System.out.println(widthMarking);
										// System.out.println(heightMarking);
										// System.out.println("=====");

										// Calculate the relative marking position considering the minimum as zero (0),
										// exclusive, and the maximum as one (1), also exclusive.
										int deltaToGuaranteeNotInclusivity = 1;
										if (x1Marking < x1Subimage) {
											x1Marking = x1Subimage + deltaToGuaranteeNotInclusivity;
										}
										if (y1Marking < y1Subimage) {
											y1Marking = y1Subimage + deltaToGuaranteeNotInclusivity;
										}
										if (widthMarking > x1Subimage + widthSubimage - x1Marking) {
											widthMarking = x1Subimage + widthSubimage - x1Marking;
										}
										if (heightMarking > y1Subimage + heightSubimage - y1Marking) {
											heightMarking = y1Subimage + heightSubimage - y1Marking;
										}
										// System.out.println(" *****");
										// System.out.println(" " + x1Subimage);
										// System.out.println(" " + y1Subimage);
										// System.out.println(" *****");
										// System.out.println(" " + x1Marking);
										// System.out.println(" " + y1Marking);
										// System.out.println(" " + widthMarking);
										// System.out.println(" " + heightMarking);
										// System.out.println(" *****");

										xCenterMarking = x1Marking + (widthMarking / 2);
										yCenterMarking = y1Marking + (heightMarking / 2);
										xCenterMarking = (xCenterMarking - x1Subimage) / widthSubimage;
										yCenterMarking = (yCenterMarking - y1Subimage) / heightSubimage;

										x1Marking = (x1Marking - x1Subimage) / widthSubimage;
										y1Marking = (y1Marking - y1Subimage) / heightSubimage;
										widthMarking /= widthSubimage;
										heightMarking /= heightSubimage;

										if (xCenterMarking < 0 || xCenterMarking > 1 || yCenterMarking < 0
												|| yCenterMarking > 1 || widthMarking < 0 || widthMarking > 1
												|| heightMarking < 0 || heightMarking > 1) {

											System.out.println(xCenterMarking + " # " + yCenterMarking + " # "
													+ widthMarking + " # " + heightMarking);

											throw new IllegalStateException(
													"Ops! An invalid coordinate has been generated.");

										}

										// Write the class index.
										if (onlyOneClass || marking.getType() == FruitType.GREEN) {
											subimageLabelContent.append("0");
										} else {
											subimageLabelContent.append("1");
										}

										// Write marking center x and center y.

										subimageLabelContent
												.append(String.format(Locale.ENGLISH, " %.6f %.6f %.6f %.6f\n",
														xCenterMarking, yCenterMarking, widthMarking, heightMarking));

									}

								}

								if (0 == subimageLabelContent.length()) {
									if (Math.random() >= PROBABILITY_OF_RETAIN_BLANK_IMAGE) {
										new File(OUTPUT_IMAGES_FOLDER + subimageName).delete();
										continue;
									}
								}

								// Randomly SELECT the image for training/test.
								if (Math.random() < PROBABILITY_OF_BEING_TRAINED_IMAGE) {
									trainContent.append(YOLO_DATA_FOLDER);
									trainContent.append(subimageName);
									trainContent.append(".jpg\n");
								} else {
									testContent.append(YOLO_DATA_FOLDER);
									testContent.append(subimageName);
									testContent.append(".jpg\n");
								}

								File outputFile = subimageLabelFile.toFile();
								outputFile.createNewFile();
								FileWriter out = new FileWriter(outputFile);
								out.write(subimageLabelContent.toString());
								out.close();

								// Add the file to the file list.
								files.add(outputFile);
							}
						}

					}
				} // ./ Does the image have at least 3000 pixels wide?

				System.out.println("          CREATED!");
			} // Is the image reviewed?

		}

		Path trainFile = Paths.get(OUTPUT_IMAGES_FOLDER, "train.txt");
		Path testFile = Paths.get(OUTPUT_IMAGES_FOLDER, "test.txt");

		// // Create and write the zip file.
		// ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		// BufferedOutputStream bufferedOutputStream = new
		// BufferedOutputStream(byteArrayOutputStream);
		// ZipOutputStream zipOutputStream = new ZipOutputStream(bufferedOutputStream);

		File trainOutputFile = trainFile.toFile();
		trainOutputFile.createNewFile();
		FileWriter out = new FileWriter(trainOutputFile);
		out.write(trainContent.toString());
		out.close();

		File testOutputFile = testFile.toFile();
		testOutputFile.createNewFile();
		out = new FileWriter(testOutputFile);
		out.write(testContent.toString());
		out.close();
		//
		// // Add the file to the file list.
		// files.add(trainOutputFile);
		// files.add(testOutputFile);
		//
		// // Pack the files.
		// for (File zipEntry : files) {
		// zipOutputStream.putNextEntry(new ZipEntry(zipEntry.getName()));
		// FileInputStream fileInputStream = new FileInputStream(zipEntry);
		//
		// IOUtils.copy(fileInputStream, zipOutputStream);
		//
		// fileInputStream.close();
		// zipOutputStream.closeEntry();
		// }
		//
		// if (zipOutputStream != null) {
		// zipOutputStream.finish();
		// zipOutputStream.flush();
		// IOUtils.closeQuietly(zipOutputStream);
		// }
		// IOUtils.closeQuietly(bufferedOutputStream);
		// IOUtils.closeQuietly(byteArrayOutputStream);
		// return byteArrayOutputStream.toByteArray();
		return "Done!";
	}

	// @RequestMapping(value = "/yolov3", produces = "application/zip")
	// public byte[] zipFiles(HttpServletResponse response) throws IOException {
	// // Set headers.
	// response.setStatus(HttpServletResponse.SC_OK);
	// response.addHeader("Content-Disposition", "attachment;
	// filename=\"yolov3.zip\"");
	//
	// ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	// BufferedOutputStream bufferedOutputStream = new
	// BufferedOutputStream(byteArrayOutputStream);
	// ZipOutputStream zipOutputStream = new ZipOutputStream(bufferedOutputStream);
	//
	// // Create temporary files for each box label file.
	// List<Image> images = galleryService.findEnabledOrderById();
	//
	// StringBuilder trainContent = new StringBuilder();
	// StringBuilder testContent = new StringBuilder();
	//
	// ArrayList<File> files = new ArrayList<>();
	// String folder = "/home/guru/Desktop/darknet/marcador/data/";
	// for (Image image : images) {
	//
	// if (image.getStatus() == Status.REVISADA) {
	//
	// if (Math.random() > 0.8) {
	// trainContent.append(folder);
	// trainContent.append(image.getName());
	// trainContent.append(".jpg\n");
	// } else {
	// testContent.append(folder);
	// testContent.append(image.getName());
	// testContent.append(".jpg\n");
	// }
	//
	// String fileName = image.getName() + ".txt";
	// Path file = getDefault().getPath("/home/leandro/marcador/output",
	// fileName);
	//
	// StringBuilder fileContent = new StringBuilder();
	//
	// List<Marking> markings = image.getMarkings();
	// for (Marking marking : markings) {
	// double x1 = marking.getX1() / 100;
	// double y1 = marking.getY1() / 100;
	// double width = marking.getWidth() / 100;
	// double height = marking.getHeight() / 100;
	//
	// double x = (x1 + width) / 2;
	// double y = (y1 + height) / 2;
	// if (x < 0 || y < 0 || x > 1 || y > 1 || (x - width) < 0 || (y - height) < 0
	// || (x + width) > 1
	// || (y + height) > 1 || 0 == x || 0 == y || 1 == x || 1 == y || 0 == width ||
	// 0 == height
	// || 1 == width || 1 == height) {
	// continue;
	// }
	//
	// if (marking.getType() == FruitType.GREEN) {
	// fileContent.append("0");
	// } else {
	// fileContent.append("1");
	// }
	// fileContent.append(" ");
	//
	// fileContent.append(x);
	// fileContent.append(" ");
	// fileContent.append(y);
	// fileContent.append(" ");
	// fileContent.append(width);
	// fileContent.append(" ");
	// fileContent.append(height);
	// fileContent.append("\n");
	// }
	//
	// File outputFile = file.toFile();
	// outputFile.createNewFile();
	// FileWriter out = new FileWriter(outputFile);
	// out.write(fileContent.toString());
	// out.close();
	//
	// // Add the file to the file list.
	// files.add(outputFile);
	// } else {
	// // testContent.append(folder);
	// // testContent.append(image.getName());
	// // testContent.append(".jpg\n");
	// }
	// }
	//
	// Path trainFile = getDefault().getPath("/home/leandro/marcador/output",
	// "train.txt");
	// Path testFile = getDefault().getPath("/home/leandro/marcador/output",
	// "test.txt");
	//
	// File trainOutputFile = trainFile.toFile();
	// trainOutputFile.createNewFile();
	// FileWriter out = new FileWriter(trainOutputFile);
	// out.write(trainContent.toString());
	// out.close();
	//
	// File testOutputFile = testFile.toFile();
	// testOutputFile.createNewFile();
	// out = new FileWriter(testOutputFile);
	// out.write(testContent.toString());
	// out.close();
	//
	// // Add the file to the file list.
	// files.add(trainOutputFile);
	// files.add(testOutputFile);
	//
	// // Pack the files.
	// for (File zipEntry : files) {
	// zipOutputStream.putNextEntry(new ZipEntry(zipEntry.getName()));
	// FileInputStream fileInputStream = new FileInputStream(zipEntry);
	//
	// IOUtils.copy(fileInputStream, zipOutputStream);
	//
	// fileInputStream.close();
	// zipOutputStream.closeEntry();
	// }
	//
	// if (zipOutputStream != null) {
	// zipOutputStream.finish();
	// zipOutputStream.flush();
	// IOUtils.closeQuietly(zipOutputStream);
	// }
	// IOUtils.closeQuietly(bufferedOutputStream);
	// IOUtils.closeQuietly(byteArrayOutputStream);
	// return byteArrayOutputStream.toByteArray();
	// }

	// private Dimension getImageDim(final String path) {
	// return this.getImageDim(path, this.getFileSuffix(path));
	// }

	private Dimension getImageDim(final String path, String suffix) {
		Dimension result = null;
		Iterator<ImageReader> iter = ImageIO.getImageReadersBySuffix(suffix);
		if (iter.hasNext()) {
			ImageReader reader = iter.next();
			try {
				ImageInputStream stream = new FileImageInputStream(new File(path));
				reader.setInput(stream);
				int width = reader.getWidth(reader.getMinIndex());
				int height = reader.getHeight(reader.getMinIndex());
				result = new Dimension(width, height);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				reader.dispose();
			}
		} else {
			System.out.println("No processor was found for " + suffix);
		}
		return result;
	}

	// private String getFileSuffix(final String path) {
	// String result = null;
	// if (path != null) {
	// result = "";
	// if (path.lastIndexOf('.') != -1) {
	// result = path.substring(path.lastIndexOf('.'));
	// if (result.startsWith(".")) {
	// result = result.substring(1);
	// }
	// }
	// }
	// return result;
	// }

	@RequestMapping(value = "/walterBode")
	public String walterBode(HttpServletResponse response) throws IOException {

		// Set response headers.
		response.setStatus(HttpServletResponse.SC_OK);

		// Get all labeled images from the database.
		List<Image> images = galleryService.findEnabledOrderById();

		for (Image image : images) {

			String originalImageURL = ORIGINAL_IMAGES_FOLDER + image.getName();
			System.out.println("Mais uma...");

			if (true) {

				Dimension dimension = getImageDim(originalImageURL, "jpg");

				int originalImageWidth = (int) dimension.getWidth();
				int originalImageHeight = (int) dimension.getHeight();

				List<Marking> markings = image.getMarkings();

				String filesWithMarkedFruits = "<annotation>\n" + "	<folder>fotos_marcadas</folder>\n"
						+ "	<filename>%file%</filename>\n"
						+ "	<path>/home/walter/Documents/fotos_marcadas/%file%</path>\n" + "	<source>\n"
						+ "		<database>Unknown</database>\n" + "	</source>\n" + "	<size>\n"
						+ "		<width>%width%</width>\n" + "		<height>%height%</height>\n"
						+ "		<depth>3</depth>\n" + "	</size>\n" + "	<segmented>0</segmented>\n" + "   %objects%"
						+ "</annotation>";

				filesWithMarkedFruits = filesWithMarkedFruits.replace("%file%", image.getName())
						.replace("%width%", String.valueOf(originalImageWidth))
						.replace("%height%", String.valueOf(originalImageHeight));
				
				String objects = "";

				for (Marking marking : markings) {

					double x1Marking = marking.getX1() * (originalImageWidth) / 100;
					double y1Marking = marking.getY1() * (originalImageHeight) / 100;
					double widthMarking = marking.getWidth() * (originalImageWidth) / 100;
					double heightMarking = marking.getHeight() * (originalImageHeight) / 100;

					String object = "	<object>\n" + "		<name>%class%</name>\n" + "		<pose>Unspecified</pose>\n"
							+ "		<truncated>0</truncated>\n" + "		<difficult>0</difficult>\n" + "		<bndbox>\n"
							+ "			<xmin>%xmin%</xmin>\n" + "			<ymin>%ymin%</ymin>\n"
							+ "			<xmax>%xmax%</xmax>\n" + "			<ymax>%ymax%</ymax>\n" + "		</bndbox>\n"
							+ "	</object>\n";

					object = object.replace("%class%", marking.getType().name());
					object = object.replace("%xmin%", String.valueOf((int)x1Marking));
					object = object.replace("%ymin%", String.valueOf((int)y1Marking));
					object = object.replace("%xmax%", String.valueOf((int)(x1Marking + widthMarking)));
					object = object.replace("%ymax%", String.valueOf((int)(y1Marking + heightMarking)));
					objects += object;
					
				}
				
				filesWithMarkedFruits = filesWithMarkedFruits.replace("%objects%", objects);

				Path fileWithMarkings = Paths.get(OUTPUT_WALTER_FOLDER, image.getName() + ".xml");
				File fileWithMarkingsFile = fileWithMarkings.toFile();
				fileWithMarkingsFile.createNewFile();
				FileWriter out = new FileWriter(fileWithMarkingsFile);
				out.write(filesWithMarkedFruits);
				out.close();

			}
		}

		return "Done!";
	}

}
