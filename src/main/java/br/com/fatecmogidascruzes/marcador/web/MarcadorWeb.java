package br.com.fatecmogidascruzes.marcador.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = { "br.com.fatecmogidascruzes" })
@EnableJpaRepositories(basePackages = { "br.com.fatecmogidascruzes" })
@EntityScan(basePackages = { "br.com.fatecmogidascruzes" })
public class MarcadorWeb extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(MarcadorWeb.class);
	}

	public static void main(String[] args) throws Exception {
		SpringApplication.run(MarcadorWeb.class, args);
	}

}
