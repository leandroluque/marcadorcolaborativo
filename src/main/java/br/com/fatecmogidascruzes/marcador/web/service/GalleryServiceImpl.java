package br.com.fatecmogidascruzes.marcador.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import br.com.fatecmogidascruzes.marcador.web.data.ImageDAO;
import br.com.fatecmogidascruzes.marcador.web.data.MarkingDAO;
import br.com.fatecmogidascruzes.marcador.web.model.Image;
import br.com.fatecmogidascruzes.marcador.web.model.Marking;
import br.com.fatecmogidascruzes.marcador.web.model.User;
import br.com.fatecmogidascruzes.marcador.web.storage.FileStorage;
import br.com.fatecmogidascruzes.marcador.web.storage.FileStorageLocal;

@Service
public class GalleryServiceImpl implements GalleryService {

	private FileStorage storage = new FileStorageLocal();

	@Autowired
	private ImageDAO imageDAO;

	@Autowired
	private MarkingDAO markingDAO;

	@Override
	@Transactional
	public void savePhoto(User user, MultipartFile file) {
		Image image = new Image();
		image.setOriginalName(file.getOriginalFilename());
		image.setContentType(file.getContentType());
		image.setSize(file.getSize());
		image.setDescription("Informe uma descrição");
		image.setUser(user);
		imageDAO.save(image);
		String fileName = "image_" + image.getId();
		storage.save(file, fileName);
		image.setName(fileName);
		imageDAO.save(image);
	}

	@Override
	public byte[] loadPhoto(Long id, Boolean gallery) {
		Image image = imageDAO.findById(id).get();
		return storage.load(image.getName(), gallery);
	}

	@Override
	public List<Image> findAll(User user) {
		return imageDAO.findByUserOrderByIdDesc(user);
	}

	@Override
	public List<Image> findEnabled(User user) {
		return imageDAO.findByUserAndEnabledTrueOrderByIdDesc(user);
	}

	@Override
	public List<Image> findEnabledOrderById() {
		return imageDAO.findByEnabledTrueOrderById();
	}

	@Override
	public Long countEnabled(User user) {
		return imageDAO.countByEnabledTrueAndUser(user);
	}

	@Override
	public Long countEnabled() {
		return imageDAO.countByEnabledTrue();
	}

	@Override
	public List<Image> findEnabled() {
		return imageDAO.findByEnabledTrueOrderByIdDesc();
	}

	@Override
	public Page<Image> findEnabled(User user, int page, int size) {
		PageRequest pageRequest = PageRequest.of(page, size);
		return imageDAO.findByUserAndEnabledTrueOrderByIdDesc(user, pageRequest);
	}

	@Override
	public Page<Image> findEnabled(int page, int size) {
		PageRequest pageRequest = PageRequest.of(page, size);
		return imageDAO.findByEnabledTrueOrderByIdDesc(pageRequest);
	}

	@Override
	public List<Image> findAll() {
		return imageDAO.findAll();
	}

	@Override
	@Transactional
	public void delete(Long id) {
		Image image = imageDAO.findById(id).get();
		deleteByImage(image);
		imageDAO.deleteById(id);
		storage.delete(image.getName());

	}

	@Override
	public Image find(Long id) {
		return imageDAO.findById(id).get();
	}

	@Override
	public void update(Image image) {
		imageDAO.save(image);
	}

	@Override
	public void addMarking(Marking marking) {
		this.markingDAO.saveAndFlush(marking);
	}

	@Override
	public void deleteMarkingByDivId(Long divId) {
		this.markingDAO.deleteByDivId(divId);
	}

	@Override
	public void deleteMarking(Long id) {
		this.markingDAO.deleteById(id);
	}

	@Override
	public void updateMarking(Marking marking) {
		this.markingDAO.saveAndFlush(marking);
	}

	@Override
	public List<Marking> findImageMarkings(Image image) {
		return this.markingDAO.findByImage(image);
	}

	@Override
	public Marking findByImageAndDivId(Image image, Long divId) {
		return this.markingDAO.findByImageAndDivId(image, divId);
	}

	public Long countByImage(Image image) {
		return this.markingDAO.countByImage(image);
	}

	public void deleteByImage(Image image) {
		this.markingDAO.deleteByImage(image);
	}
}
