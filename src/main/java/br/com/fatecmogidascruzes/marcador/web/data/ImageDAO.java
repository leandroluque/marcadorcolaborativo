package br.com.fatecmogidascruzes.marcador.web.data;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import br.com.fatecmogidascruzes.marcador.web.model.Image;
import br.com.fatecmogidascruzes.marcador.web.model.User;

public interface ImageDAO extends JpaRepository<Image, Long> {

	public List<Image> findByUserOrderByIdDesc(User user);

	public List<Image> findByUserAndEnabledTrueOrderByIdDesc(User user);

	public Page<Image> findByUserOrderByIdDesc(User user, Pageable page);

	public Page<Image> findByUserAndEnabledTrueOrderByIdDesc(User user, Pageable page);

	public Page<Image> findByEnabledTrueOrderByIdDesc(Pageable page);

	public List<Image> findByEnabledTrueOrderByIdDesc();
	
	public List<Image> findByEnabledTrueOrderById();

	public Long countByEnabledTrue();

	public Long countByEnabledTrueAndUser(User user);

}
