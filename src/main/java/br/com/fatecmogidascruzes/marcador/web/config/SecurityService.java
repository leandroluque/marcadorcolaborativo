package br.com.fatecmogidascruzes.marcador.web.config;

public interface SecurityService {

    public String findByEmail();

    public void autologin(String email, String password);
	
}
