package br.com.fatecmogidascruzes.marcador.web.service;

import java.util.Date;

import br.com.fatecmogidascruzes.marcador.web.model.User;

public interface UserService {

	void save(User user);

	User findByEmail(String email);
	
	Long count();
	
	Long countCreatedAfter(Date date);

}