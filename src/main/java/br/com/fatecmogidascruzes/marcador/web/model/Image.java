package br.com.fatecmogidascruzes.marcador.web.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@javax.persistence.Entity
@Table(name = "img_gallery")
public class Image extends Entity {

	@Column(name = "original_name")
	private String originalName;
	@Column(name = "name")
	private String name;
	@Column(name = "content_type")
	private String contentType;
	@Column(name = "size")
	private Long size;
	@Column(name = "description")
	private String description;
	@ManyToOne
	@JoinColumn(name = "user")
	private User user;
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "image")
	private List<Marking> markings = new ArrayList<>();
	// TODO: move to TO.
	@Transient
	private Long howManyMarkings;

	public enum Status {
		REVISADA, REVISAR, VALIDADA
	};

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private Status status;

	public Image() {
		super();
	}

	public Image(String name) {
		super();
		this.name = name;
	}

	public String getOriginalName() {
		return originalName;
	}

	public void setOriginalName(String originalName) {
		this.originalName = originalName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Long getHowManyMarkings() {
		return howManyMarkings;
	}

	public void setHowManyMarkings(Long howManyMarkings) {
		this.howManyMarkings = howManyMarkings;
	}

	public List<Marking> getMarkings() {
		return markings;
	}

	public void setMarkings(List<Marking> markings) {
		this.markings = markings;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
