package br.com.fatecmogidascruzes.marcador.web.to;

import br.com.fatecmogidascruzes.marcador.web.model.Marking;

public class MarkingTO {

	private Long divId;
	private Double x1;
	private Double y1;
	private Double width;
	private Double height;
	private String type;

	public MarkingTO() {
		super();
	}

	public MarkingTO(Marking marking) {
		this.divId = marking.getDivId();
		this.x1 = marking.getX1();
		this.y1 = marking.getY1();
		this.width = marking.getWidth();
		this.height = marking.getHeight();
		this.type = marking.getType().name();
	}

	public Long getDivId() {
		return divId;
	}

	public void setDivId(Long divId) {
		this.divId = divId;
	}

	public Double getX1() {
		return x1;
	}

	public void setX1(Double x1) {
		this.x1 = x1;
	}

	public Double getY1() {
		return y1;
	}

	public void setY1(Double y1) {
		this.y1 = y1;
	}

	public Double getWidth() {
		return width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
