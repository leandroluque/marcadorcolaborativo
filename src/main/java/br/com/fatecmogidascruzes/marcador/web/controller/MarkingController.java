package br.com.fatecmogidascruzes.marcador.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.fatecmogidascruzes.marcador.web.model.Marking;
import br.com.fatecmogidascruzes.marcador.web.model.Marking.FruitType;
import br.com.fatecmogidascruzes.marcador.web.service.GalleryService;
import br.com.fatecmogidascruzes.marcador.web.to.MarkingResponse;

@RestController
@RequestMapping("/marcacoes/")
public class MarkingController {

	private final GalleryService galleryService;

	@Autowired
	public MarkingController(GalleryService galleryService) {
		super();
		this.galleryService = galleryService;
	}

	@RequestMapping("/adicionar/{imageId}/{divId}")
	public MarkingResponse add(@PathVariable Long imageId, @PathVariable Long divId, @RequestParam("x1") Double x1, @RequestParam("y1") Double y1,
			@RequestParam("width") Double width, @RequestParam("height") Double height,
			@RequestParam("type") String type) {

		try {
			Marking marking = new Marking();
			marking.setX1(x1);
			marking.setY1(y1);
			marking.setWidth(width);
			marking.setHeight(height);
			marking.setDivId(divId);
			marking.setImage(this.galleryService.find(imageId));
			marking.setType(type.equalsIgnoreCase("ripe") ? FruitType.RIPE : FruitType.GREEN);

			galleryService.addMarking(marking);
			
			return new MarkingResponse("ok", divId);
		} catch (Exception error) {
			error.printStackTrace();
			return new MarkingResponse("error");
		}
	}


	@RequestMapping("/alterar/{imageId}/{divId}")
	public MarkingResponse update(@PathVariable Long imageId, @PathVariable Long divId, @RequestParam("x1") Double x1, @RequestParam("y1") Double y1,
			@RequestParam("width") Double width, @RequestParam("height") Double height) {

		try {
			Marking marking = galleryService.findByImageAndDivId(this.galleryService.find(imageId), divId);
			marking.setX1(x1);
			marking.setY1(y1);
			marking.setWidth(width);
			marking.setHeight(height);
			galleryService.updateMarking(marking);
			
			return new MarkingResponse("ok", divId);
		} catch (Exception error) {
			error.printStackTrace();
			return new MarkingResponse("error");
		}
	}
	
	@RequestMapping("/remover/{imageId}/{divId}")
	public MarkingResponse remove(@PathVariable Long imageId, @PathVariable Long divId) {

		try {
			Marking marking = galleryService.findByImageAndDivId(this.galleryService.find(imageId), divId);

			galleryService.deleteMarking(marking.getId());
			
			return new MarkingResponse("ok", divId);
		} catch (Exception error) {
			error.printStackTrace();
			return new MarkingResponse("error");
		}
	}
}
