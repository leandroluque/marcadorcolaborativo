package br.com.fatecmogidascruzes.marcador.web.data;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.fatecmogidascruzes.marcador.web.model.User;

public interface UserDAO extends JpaRepository<User, Long> {

	public User findByEmail(String email);
	
	public Long countByCreationAfter(Date date);

}
