package br.com.fatecmogidascruzes.marcador.web.storage;

import org.springframework.web.multipart.MultipartFile;

public interface FileStorage {

	public void save(MultipartFile file, String name);

	public String save(MultipartFile file);

	public byte[] load(String name, Boolean gallery);

	public void delete(String name);

}
