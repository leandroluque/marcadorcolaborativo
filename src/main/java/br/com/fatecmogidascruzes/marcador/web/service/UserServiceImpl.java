package br.com.fatecmogidascruzes.marcador.web.service;

import java.util.Date;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.fatecmogidascruzes.marcador.web.data.RoleDAO;
import br.com.fatecmogidascruzes.marcador.web.data.UserDAO;
import br.com.fatecmogidascruzes.marcador.web.model.User;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
    private UserDAO userRepository;
    @Autowired
    private RoleDAO roleRepository;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setRoles(new HashSet<>(roleRepository.findAll()));
        userRepository.save(user);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }
    
    @Override
    public Long count() {
    	return userRepository.count();
    }
    
    @Override
    public Long countCreatedAfter(Date date) {
    	return userRepository.countByCreationAfter(date);
    }
	
}
