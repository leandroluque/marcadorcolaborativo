package br.com.fatecmogidascruzes.marcador.web.controller;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.fatecmogidascruzes.marcador.web.data.MarkingDAO;
import br.com.fatecmogidascruzes.marcador.web.model.Image;
import br.com.fatecmogidascruzes.marcador.web.model.Marking;
import br.com.fatecmogidascruzes.marcador.web.model.Marking.FruitType;
import br.com.fatecmogidascruzes.marcador.web.service.GalleryService;

//@RestController
//@RequestMapping("/adjustFruits")
public class AdjusteFruitTones {
/*
	private static final String ORIGINAL_IMAGES_FOLDER = "/home/leandro/workspace/marcador/fotos-marcadas/";

	private final GalleryService galleryService;
	private final MarkingDAO markingDAO;

	private int[] selectedImages = {370, 362, 363, 364, 366, 368, 397, 426, 427, 429, 437, 438, 447, 494, 521,
			529, 530, 533, 551, 601, 625, 691, 695, 706, 732, 734, 765, 766, 817, 818, 819, 824, 854, 866, 895, 903,
			906, 913, 914, 920, 924, 982, 1013, 1056, 1143, 1149, 1186, 1202, 1214, 1215, 1217, 1218};

	@Autowired
	public AdjusteFruitTones(GalleryService galleryService, MarkingDAO markingDAO) {
		super();
		this.galleryService = galleryService;
		this.markingDAO = markingDAO;
	}

	@RequestMapping
	public @ResponseBody String adjust() throws IOException {

		Arrays.sort(selectedImages);

		// Get all labeled images from the database.
		List<Image> images = galleryService.findEnabledOrderById();

		for (Image image : images) {
			
			if(Arrays.binarySearch(selectedImages, image.getId().intValue()) < 0) {
				continue;
			}
			
			System.out.println("Processing image " + image.getId());

			String originalImageURL = ORIGINAL_IMAGES_FOLDER + image.getName();

			BufferedImage imageFile = ImageIO.read(new File(originalImageURL));
			int originalImageWidth = imageFile.getWidth();
			int originalImageHeight = imageFile.getHeight();

			// For each marking, check whether the marking has more than
			List<Marking> markings = image.getMarkings();

			for (Marking marking : markings) {

				// Calculate the marking absolute position considering the original image size
				// with zoom.
				double x1Marking = marking.getX1() * (originalImageWidth) / 100;
				double y1Marking = marking.getY1() * (originalImageHeight) / 100;
				double widthMarking = marking.getWidth() * (originalImageWidth) / 100;
				double heightMarking = marking.getHeight() * (originalImageHeight) / 100;

				// Check whether the image has 40% or more yellow points.
				int howManyYellow = 0;
				for (double x = Math.max(0, x1Marking); x < x1Marking + widthMarking && x < originalImageWidth; x++) {
					for (double y = Math.max(0,  y1Marking); y < y1Marking + heightMarking && y < originalImageHeight; y++) {
						int pixel = imageFile.getRGB((int) x, (int) y);
						Color cor = new Color(pixel);
						int r = cor.getRed();
						int g = cor.getGreen();
						int b = cor.getBlue();

						float[] hsv = new float[3];
						Color.RGBtoHSB(r, g, b, hsv);

						int h = (int)(300 * hsv[0]);
						if (h >= 30 && h < 62) {
							howManyYellow++;
						}
					}
				}

			//	System.out.println(howManyYellow / (widthMarking * heightMarking));
				if (howManyYellow / (widthMarking * heightMarking) >= 0.4) {
					marking.setType(FruitType.RIPE);
				} else {
					marking.setType(FruitType.GREEN);
				}

				markingDAO.save(marking);

			}
			
		//	System.exit(-1);
		}

		return "Done!";
	}*/

}
