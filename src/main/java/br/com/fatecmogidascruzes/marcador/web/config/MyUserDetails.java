package br.com.fatecmogidascruzes.marcador.web.config;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

public class MyUserDetails extends User {

	private static final long serialVersionUID = 1L;

	public MyUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities,
			String fullName, String memberSince) {
		super(username, password, authorities);
		this.fullName = fullName;
		this.memberSince = memberSince;
	}

	public MyUserDetails(String fullName, String memberSince, String username, String password, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired, boolean accountNonLocked,
			Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		this.fullName = fullName;
		this.memberSince = memberSince;
	}

	private String fullName;
	private String memberSince;

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getMemberSince() {
		return memberSince;
	}

	public void setMemberSince(String memberSince) {
		this.memberSince = memberSince;
	}

}
