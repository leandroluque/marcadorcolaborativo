package br.com.fatecmogidascruzes.marcador.web.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@javax.persistence.Entity
@Table(name = "img_marking")
public class Marking extends Entity {

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "image")
	private Image image;
	@Basic
	@Column(name = "x1")
	private Double x1;
	@Basic
	@Column(name = "y1")
	private Double y1;
	@Basic
	@Column(name = "width")
	private Double width;
	@Basic
	@Column(name = "height")
	private Double height;
	@Enumerated(EnumType.STRING)
	@Column(name = "type")
	private FruitType type;
	@Basic
	@Column(name = "div_id")
	private Long divId;

	public enum FruitType {
		RIPE, GREEN
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public Double getX1() {
		return x1;
	}

	public void setX1(Double x1) {
		this.x1 = x1;
	}

	public Double getY1() {
		return y1;
	}

	public void setY1(Double y1) {
		this.y1 = y1;
	}

	public Double getWidth() {
		return width;
	}

	public void setWidth(Double x2) {
		this.width = x2;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double y2) {
		this.height = y2;
	}

	public FruitType getType() {
		return type;
	}

	public void setType(FruitType type) {
		this.type = type;
	}

	public Long getDivId() {
		return divId;
	}

	public void setDivId(Long divId) {
		this.divId = divId;
	};
	
}
