package br.com.fatecmogidascruzes.marcador.web.storage;

import static java.nio.file.FileSystems.getDefault;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.imageio.ImageIO;

import org.imgscalr.Scalr;
import org.imgscalr.Scalr.Mode;
import org.imgscalr.Scalr.Rotation;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;

//@Profile("local-storage")
@Service
public class FileStorageLocal implements FileStorage {

	private Path path;

	private Path thumbPath;

	private BufferedImage bufferedImage;

	private int orientation;

	final private int HEIGHT = 200;
	final private int WIDITH = 200;

	public FileStorageLocal() {
		this.path = getDefault().getPath("/home/leandro/marcador", ".fotos");

		try {
			Files.createDirectories(path);
		} catch (IOException error) {
			throw new RuntimeException("Error creating local files folder.", error);
		}

		this.thumbPath = getDefault().getPath("/home/leandro/marcador", ".thumbnails");

		try {
			Files.createDirectories(thumbPath);
		} catch (IOException error) {
			throw new RuntimeException("Error creating local thumbnails folder.", error);
		}
	}

	@Override
	public String save(MultipartFile file) {
		save(file, file.getName());
		return file.getName();
	}

	@Override
	public void save(MultipartFile file, String name) {
		try {
			Metadata metadata = ImageMetadataReader.readMetadata(file.getInputStream());
			ExifIFD0Directory exifDir = metadata.getFirstDirectoryOfType(ExifIFD0Directory.class);
			orientation = exifDir.getInteger(ExifIFD0Directory.TAG_ORIENTATION);
			bufferedImage = ImageIO.read(file.getInputStream());
			/*switch (orientation) {
			case 2:
				bufferedImage = Scalr.rotate(bufferedImage, Rotation.FLIP_HORZ);
				break;
			case 3:
				bufferedImage = Scalr.rotate(bufferedImage, Rotation.CW_180);
				break;
			case 4:
				bufferedImage = Scalr.rotate(bufferedImage, Rotation.FLIP_VERT);
				break;
			case 6:
				bufferedImage = Scalr.rotate(bufferedImage, Rotation.CW_90);
				break;
			case 8:
				bufferedImage = Scalr.rotate(bufferedImage, Rotation.CW_270);
				break;
			}*/
			ImageIO.write(bufferedImage, "jpg",
					new File(this.path.toAbsolutePath().toString() + getDefault().getSeparator() + name));
			bufferedImage = Scalr.resize(bufferedImage, Mode.AUTOMATIC, HEIGHT, WIDITH);
			ImageIO.write(bufferedImage, "jpg",
					new File(this.thumbPath.toAbsolutePath().toString() + getDefault().getSeparator() + name));

		} catch (ImageProcessingException error) {
			System.out.println(".....: " + error.getMessage());
			throw new RuntimeException("Error saving local file.", error);
		} catch (IOException error) {
			System.out.println(".....: " + error.getMessage());
			throw new RuntimeException("Error saving local file.", error);
		}

		/*
		 * try { file.transferTo(new File(this.path.toAbsolutePath().toString() +
		 * getDefault().getSeparator() + name)); } catch (IOException error) {
		 * System.out.println(".....: " + error.getMessage()); throw new
		 * RuntimeException("Error saving local file.", error); }
		 */
	}

	@Override
	public byte[] load(String name, Boolean gallery) {
		try {
			return gallery ? Files.readAllBytes(this.thumbPath.resolve(name))
					: Files.readAllBytes(this.path.resolve(name));
		} catch (IOException error) {
			throw new RuntimeException("Error loading file.", error);
		}
	}

	@Override
	public void delete(String name) {
		try {
			Files.delete(this.path.resolve(name));
			Files.delete(this.thumbPath.resolve(name));
		} catch (IOException error) {
			throw new RuntimeException("Error removing local file.", error);
		}
	}

}
