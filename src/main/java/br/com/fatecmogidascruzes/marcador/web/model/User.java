package br.com.fatecmogidascruzes.marcador.web.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@javax.persistence.Entity
@Table(name = "user")
public class User extends Entity {

	@Column(name = "cep")
	@NotBlank(message = "CEP é obrigatório")
	private String cep;
	@NotBlank(message = "Nome é obrigatório")
	@Column(name = "name")
	private String name;

	public enum Gender {
		MALE, FEMALE
	};

	@NotNull(message = "Sexo é obrigatório")
	@Column(name = "gender")
	@Enumerated(EnumType.STRING)
	private Gender gender;
	@Column(name = "birth_date")
	@NotNull(message = "Data de nascimento é obrigatória")
	private Date birthDate;

	public enum EducationLevel {
		ELEMENTARY_INCOMPLETE, ELEMENTARY_COMPLETE, MIDDLE_INCOMPLETE, MIDDLE_COMPLETE, HIGH_INCOMPLETE, HIGH_COMPLETE, UNDERGRADUATE_INCOMPLETE, UNDERGRADUATE_COMPLETE, GRADUATE_INCOMPLETE, GRADUATE_COMPLETE, MASTER, PHD
	};

	@Column(name = "edu_level")
	@Enumerated(EnumType.STRING)
	private EducationLevel academicLevel;
	@NotBlank(message = "E-mail é obrigatório")
	@Column(name = "email")
	private String email;
	@NotBlank(message = "Senha é obrigatória")
	@Column(name = "password")
	private String password;
	@Transient
	private String passwordConfirm;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "user_role", joinColumns = @JoinColumn(name = "user"), inverseJoinColumns = @JoinColumn(name = "role"))
	private Set<Role> roles;

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public EducationLevel getAcademicLevel() {
		return academicLevel;
	}

	public void setAcademicLevel(EducationLevel academicLevel) {
		this.academicLevel = academicLevel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

}