$(function() {

	var contextPath = $("meta[name='_context_path']").attr("content");
	var csrfHeader = "{ \"'" + $("meta[name='_csrf_header']").attr("content")
			+ "\" : \"" + $("meta[name='_csrf']").attr("content") + "\" }";

	var uploadArea = $("#uploadArea");
	var progressbar = $("#progressbar");
	var bar = progressbar.find('.uk-progress-bar');

	var settings = {
		type : "json",
		// filelimit : 1,
		multiple : true,
		allow : "*.(jpg|jpeg|png|gif)",
		action : contextPath + "galeria/nova",
		params : {
			_csrf : $("meta[name='_csrf']").attr("content")
		},
		loadstart : function() {
			bar.css("width", "0%").text("0%");
			progressbar.removeClass("uk-hidden");
		},
		progress : function(percent) {
			percent = Math.ceil(percent);
			bar.css("width", percent + "%").text(percent + "%");
		},
		allcomplete : function(response) {
			bar.css("width", "100%").text("100%");

			setTimeout(function() {
				progressbar.addClass("uk-hidden");
			}, 250);

			location.reload(false);
		}
	};

	UIkit.uploadSelect("#uploadSelect", settings);
	UIkit.uploadDrop(uploadArea, settings);

	$(".deleteAction").click(function() {
		$("#deletionId").val($(this).data("server"));
	});

	$(".changeAction").click(function() {
		$("#changeId").val($(this).data("server"))
		$("#description").val($(this).data("description"))
	});

	$('#changeDescription').on('shown.bs.modal', function() {
		$('#description').focus();
	});

});